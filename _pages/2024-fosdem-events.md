---
title: "2024 FOSDEM Events"
layout: single
permalink: /2024-fosdem-events
---

What's happening during and around FOSDEM 2024?

Add events here, ordered by date, and include the datetime, location, and a link to the event.

You can update the list by sending a merge request for [this file in the website repository](https://gitlab.com/flossfoundations/flossfoundations/-/blob/master/_pages/2024-fosdem-events.md).

<!--
* [event name](link)
    * date and time
    * location
-->
## Monday, 29 January 2024

<!--
* [event name](link)
    * date and time
    * location
-->

## Tuesday, 30 January 2024

<!--
* [event name](link)
    * date and time
    * location
-->

## Wednesday, 31 January 2024

<!--
* [event name](link)
    * date and time
    * location
-->

## Thursday, 01 February 2024

<!--
* [event name](link)
    * date and time
    * location
-->

## Friday, 02 February 2024

<!--
* [event name](link)
    * date and time
    * location
-->

## Saturday, 03 February 2024 

* [FOSDEM!](https://fosdem.org/2024/)
    * 03-04 February, 2024
    * Université libre de Bruxelles, Campus du Solbosch, Avenue Franklin D. Roosevelt 50, 1050 Bruxelles, Belgium ([transportation](https://fosdem.org/2024/practical/transportation/))

<!--
* [event name](link)
    * date and time
    * location
-->

## Sunday, 04 February 2024

* [FOSDEM!](https://fosdem.org/2024/)
    * 03-04 February, 2024
    * Université libre de Bruxelles, Campus du Solbosch, Avenue Franklin D. Roosevelt 50, 1050 Bruxelles, Belgium ([transportation](https://fosdem.org/2024/practical/transportation/))

<!--
* [event name](link)
    * date and time
    * location
-->

## Monday, 05 February 2024

<!--
* [event name](link)
    * date and time
    * location
-->
